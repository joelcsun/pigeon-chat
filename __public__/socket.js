const ws = new WebSocket('ws://localhost:3000');
const EVENT_HANDLERS = {};

function sendEvent (event, data) {
  ws.send(
    JSON.stringify({ event, data })
  );
};

function registerEvent (event, handler) {
  EVENT_HANDLERS[event] = handler;
}

ws.onmessage = function (wsEvent) {
  if(!wsEvent || !wsEvent.data)
    return;

  const { event, data } = JSON.parse(wsEvent.data);

  console.log('ws:', event);

  const handler = EVENT_HANDLERS[event || ''];
  handler && handler(data);

};

ws.onerror = function(err){
  console.error('SOCKET ERROR', err);
};
